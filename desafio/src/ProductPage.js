import React, { Component } from 'react';


class ProductPage extends React.Component {
    state = {
        isOn: false,
        page: 3
    }

    changeProduct = sku => {
        this.setState({
            page: sku
        })
    }

    render () {
        const { product } = this.props

        console.log(this)
        return (
        <div >
        <div class="header">Desafio 2</div>
        <div class="head">
        <strong>{product.skus[this.state.page].skuname}</strong> <br/>
        <span class="detalhes">(Cód.{product.skus[this.state.page].sku} )</span> 
        </div>
        <div class="left"> <img src={product.skus[this.state.page].image} width="500" height="500"></img></div>
        <div class="right1">
        <spam class="detalhes">vendido por {product.skus[this.state.page].seller}</spam> <br/>
        <spam class="detalhes"><strike>R$ {product.skus[this.state.page].listPrice}</strike>(0% de desconto)</spam> <br/>
        <spam><h1>R${product.skus[this.state.page].listPrice}</h1></spam> <br/>
        <button class="button" id="btn5" onClick="alert('Adicionado ao Carrinho')">Compar</button>
        </div>
        <div class="right2">{product.dimensionsMap.tamanho[this.state.page]}</div>
        <div class="galery">
        <spam class="detalhes"><strong> Tamanhos Disponiveis</strong></spam> <br/>
        <button class="button" type="submit" id="btn1" onClick={() => {
            this.changeProduct(0)
        }}>P</button>
        <button type="submit" class="button" id="btn2" onClick={() => {
            this.changeProduct(1)
        }}>M</button>
        <button class="button" id="btn3" onClick={() => {
            this.changeProduct(2)
        }}>G</button>
        <button class="button" id="btn4" onClick={() => {
            this.changeProduct(3)
        }}>GG</button>
        </div>
        </div>
        )
    }
}

export default ProductPage
